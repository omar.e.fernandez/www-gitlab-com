- name: Estimated Combined Monthly Active Users (CMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: The estimated sum of all stage monthly active users (<a href="https://about.gitlab.com/handbook/product/performance-indicators/#stage-monthly-active-users-smau">SMAU</a>)
    in a rolling 28 day period for Self-Managed and SaaS. We extrapolate estimated CMAU based on the percentage of self-managed instances that report this data (not all self-managed instances do).  Therefore the recorded number is not the same as the estimated or even the actual number. The chart currently reflects recorded CMAU. We plan to change this to estimated CMAU [soon](https://gitlab.com/gitlab-data/analytics/-/issues/6000). Numbers displayed below are for self-managed instances
    with usage ping (<a href="https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=8462059&udv=0">enabled</a>),
    including the self-managed instance hosting the SaaS product.
    Data for (<a href="https://app.periscopedata.com/app/gitlab/604621/GitLab.com-Postgres-SMAU?widget=9128022&udv=0">CMAU for SaaS</a>)
    is available separately using GitLab.com Postgres Database Import. Owner of this KPI is the VP, Product Management.
  target: TBD
  org: Product
  public: true
  is_key: true
  health:
    level: 0
    reasons:
    - See notes in <a href="https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg?usp=sharing">Key Review slides</a>
  sisense_data:
    chart: 10291414
    dashboard: 771580
    embed: v2
  data_source:
    - version_db
  dbt_models:
    - mart_monthly_product_usage
- name: Paid Combined Monthly Active Users (Paid CMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: The sum of all (<a href="https://about.gitlab.com/handbook/product/performance-indicators/#paid-stage-monthly-active-users-paid-smau">paid stage monthly active users </a>) in a 28 day rolling period. The license information used to generate this metric from usage ping is being validated, and the currently displayed data could be incorrect by up to 20%. Numbers displayed below are for self-managed instances with usage ping(<a href="https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=8462059&udv=0">enabled</a>). Data for (<a href="https://app.periscopedata.com/app/gitlab/604621/GitLab.com-Postgres-SMAU?widget=9279567&udv=964756">Paid CMAU for SaaS </a>) is available separately using GitLab.com Postgres Database Import. Owner of this KPI is the VP, Product Management.
  target: TBD
  org: Product
  public: true
  is_key: true
  health:
    level: 0
    reasons:
    - See notes in <a href="https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg?usp=sharing">Key Review slides</a>
  sisense_data:
    chart: 10288750
    dashboard: 771580
    embed: v2
  data_source:
    - version_db
    - gitlab_dotcom
  dbt_models:
    - mart_monthly_product_usage

- name: Unique Monthly Active Users (UMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique users that performed an <a href="https://docs.gitlab.com/ee/api/events.html">event</a> within the previous 28 days for Self-Managed and SaaS. Numbers displayed below are for self-managed instances with usage ping (<a href="https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=8462059&udv=0">enabled</a>) and GitLab.com. Data for (<a href="https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=9542820&udv=0"> Paid Self-Managed (EE) Unique Monthly Active Users (UMAU) (28-day trailing window) </a>). Data for (<a href="https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=8079819&udv=0"> SaaS GitLab.com Unique Monthly Active Users (UMAU) (28-day trailing window) </a>) is also available using GitLab.com Postgres Database Import. UMAU calculation is the same using both data sources (usage ping and GitLab.com Postgres Database Import). Owner of this KPI is the VP, Product Management.
  target: TBD
  org: Product
  public: true
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in <a href="https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg?usp=sharing">Key Review slides</a>
  sisense_data:
    chart: 10288955
    dashboard: 771580
    embed: v2
  sisense_data_secondary:
    chart: 10303316
    dashboard: 771580
    embed: v2
  data_source:
    - version_db
    - gitlab_dotcom
  dbt_models:
    - mart_monthly_product_usage
    - gitlab_dotcom_daily_usage_data_events

- name: New Group Namespace Trial to Paid Conversion Rate
  base_path: "/handbook/product/performance-indicators/"
  definition: The conversion rate of a new group namespace on a trial to a <a href="https://about.gitlab.com/handbook/product/performance-indicators/#paid-namespace">paid namespace</a>. This is calculated by dividing the number of eligible new group namespaces that were on a trial and converted to paid within 40 days of creation by the total number of eligible new group namespaces that were on a trial within 40 days of creation. Eligible namespaces are top-level namespaces, excluding personal namespaces. The data source is GitLab.com Postgres Database Import. The owner of this KPI is the Director of PM, Growth.
  target: See notes in <a href="https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg?usp=sharing">Key Review slides</a>
  org: Product
  public: false
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in <a href="https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg?usp=sharing">Key Review slides</a>
  urls:
    - https://app.periscopedata.com/app/gitlab/761347/New-Growth-KPIs-(WIP)
  data_source:
    - gitlab_dotcom
    - customers_db
  dbt_models:
    - customers_db_charges_xf
    - gitlab_dotcom_namespaces_xf
    - gitlab_dotcom_memberships

- name: New Group Namespace Create Stage Adoption Rate
  base_path: "/handbook/product/performance-indicators/"
  definition: The adoption rate of a new group namespace adopting features on  <a href="https://about.gitlab.com/handbook/product/categories/#create-stage">the Create stage </a>. This is calculated as the % of eligible new group namespaces the have at least one Create SMAU action within the first 7 days of creation. Eligible namespaces are top-level namespaces, excluding personal namespaces. The data source is GitLab.com Postgres Database Import. The owner of this KPI is the Director of PM, Growth.
  target: 42%
  org: Product
  public: false
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in <a href="https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg?usp=sharing">Key Review slides</a>
  urls:
    - https://app.periscopedata.com/app/gitlab/761347/New-Growth-KPIs-(WIP)
  data_source:
    - gitlab_dotcom
    - customers_db
  dbt_models:
    - customers_db_charges_xf
    - gitlab_dotcom_namespaces_xf
    - gitlab_dotcom_memberships
    - gitlab_dotcom_daily_usage_data_events



- name: New Group Namespace Verify Stage Adoption Rate
  base_path: "/handbook/product/performance-indicators/"
  definition: The adoption rate of a new group namespace adopting features on  <a href="https://about.gitlab.com/handbook/product/categories/#verify-stage">the Verify stage </a>. This is calculated as the % of eligible new group namespaces the have at least one Verify SMAU action within the first 90 days of creation. Eligible namespaces are top-level namespaces, excluding personal namespaces. The data source is GitLab.com Postgres Database Import. The owner of this KPI is the Director of PM, Growth.
  target: SaaS is TBD (FY21 Q4 focus). 
  org: Product
  public: false
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in <a href="https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg?usp=sharing">Key Review slides</a>
  urls:
    - https://app.periscopedata.com/app/gitlab/761347/New-Growth-KPIs-(WIP)
  data_source:
    - gitlab_dotcom
    - customers_db
  dbt_models:
    - customers_db_charges_xf
    - gitlab_dotcom_namespaces_xf
    - gitlab_dotcom_memberships
    - gitlab_dotcom_daily_usage_data_events

- name: New Group Namespace with at least two users added
  base_path: "/handbook/product/performance-indicators/"
  definition: The percentage of a new group namespace with at least two users added within 7 days of namespace creation. Eligible namespaces are top-level namespaces, excluding personal namespaces. User counts for top level namespaces can come from <a href="https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.gitlab_dotcom_memberships">multiple avenues</a> including sub-groups or projects. The data source is GitLab.com Postgres Database Import. The owner of this KPI is the Director of PM, Growth.
  target: See notes in <a href="https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg?usp=sharing">Key Review slides</a> 
  org: Product
  public: false
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in <a href="https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg?usp=sharing">Key Review slides</a>
  urls:
    - https://app.periscopedata.com/app/gitlab/761347/New-Growth-KPIs-(WIP)
  data_source:
    - gitlab_dotcom
  dbt_models:
    - gitlab_dotcom_namespaces_xf
    - gitlab_dotcom_memberships
    - gitlab_dotcom_daily_usage_data_events

- name: Stages per User (SpU)
  base_path: "/handbook/product/performance-indicators/"
  definition: Stages per user is calculated by dividing <a href="https://about.gitlab.com/handbook/product/performance-indicators/#estimated-combined-monthly-active-users">Stage
    Monthly Active User (CMAU)</a>  by <a href="https://about.gitlab.com/handbook/product/performance-indicators/#unique-monthly-active-users-umau">Monthly
    Active Users (UMAU)</a>. SpU is now calculated using solely Usage Ping Data Source. As Stages add new SMAU counters and instances upgrade to newer versions, this KPI will keep on increasing artificially and will stabilize beginning of Q2 2021. The Stages per User (SpU) KPI is meant to capture the
    number of DevOps stages the average user is using on a monthly basis. This metric
    is important, as each stage added <a href="https://about.gitlab.com/direction/#strategic-response">triples
    paid conversion</a>.  Owner of this KPI is the VP, Product Management.
  target: Our Target SpU is 1.8.
  org: Product
  public: true
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in <a href="https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg?usp=sharing">Key Review slides</a>
  sisense_data:
    chart: 10184022
    dashboard: 771580
    embed: v2
  data_source:
    - version_db
  dbt_models:
    - mart_monthly_product_usage

- name: Stages per Organization (SpO)
  base_path: "/handbook/product/performance-indicators/"
  definition: Stages per Organization is calculated by dividing the sum of Stage Monthly Active Organizations (with a Stage Monthly Active Organization being defined as an instance for Self-Managed or a Namespace for SaaS with at least one Stage Active User on a given month) by the number of Unique Monthly Active Organizations (with UMAO being defined as an Organization with at least one UMAU). The Stages per Organization (SpO) KPI is meant to capture the number of DevOps stages the average organization is using on a monthly basis. This metric is important, as each stage added <a href="https://about.gitlab.com/direction/#strategic-response">triples paid conversion</a>.  Owner of this KPI is the VP, Product Management.
  target: undefined
  org: Product
  public: true
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in <a href="https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg?usp=sharing">Key Review slides</a>
  sisense_data:
    chart: 10698685
    dashboard: 771580
    embed: v2
  sisense_data_secondary:
    chart: 10955761
    dashboard: 771580
    embed: v2
  sisense_data_tertiary:
    chart: 10955277
    dashboard: 771580
    embed: v2
  data_source:
    - version_db
    - gitlab_dotcom
  dbt_models:
    - mart_monthly_product_usage
    - wk_spo
- name: Category Maturity Advancement
  base_path: "/handbook/product/performance-indicators/"
  definition: Number of category maturity advancements per quarter. We have a three year goal of having half of our categories at lovable maturity.  It's important that we see steady advancement of maturity each quarter in order to achieve the larger goal.  Owner of this KPI is the VP, Product Management.
  target: 5 category maturity advancements per quarter
  org: Product
  public: true
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in <a href="https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg?usp=sharing">Key Review slides</a>
  sisense_data:
    chart: 11735141
    dashboard: 865449
    embed: v2
  data_source:
    - categories_yaml_historical

- name: Paid Net Promoter Score (PNPS)
  base_path: "/handbook/product/performance-indicators/"
  definition: PNPS is the primary measurement of customer satisfaction with the GitLab product. Abbreviated as the PNPS acronym, please do not refer to it as NPS to
    prevent confusion. Measured as the percentage of paid customer "promoters" minus
    the percentage of paid customer "detractors" from a <a href="https://en.wikipedia.org/wiki/Net_Promoter#targetText=Net%20Promoter%20or%20Net%20Promoter,be%20correlated%20with%20revenue%20growth">Net
    Promoter Score</a> survey.  Note that while other teams at GitLab use a <a href="https://about.gitlab.com/handbook/business-ops/data-team/metrics/#satisfaction">satisfaction
    score</a>, we have chosen to use PNPS in this case so it is easier to benchmark
    versus other like companies.  Also note that the score will likely reflect customer
    satisfaction beyond the product itself, as customers will grade us on the total
    customer experience, including support, documentation, billing, etc. Owner of
    this KPI is Product Operations.
  target: Our Target PNPS is 40.
  org: Product
  public: true
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in <a href="https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg?usp=sharing">Key Review slides</a>
  sisense_data:
    chart: 8521210
    dashboard: 527913
    embed: v2
  sisense_data_secondary:
    chart: 9184725
    dashboard: 527913
    embed: v2
  data_source:
    - qualtrics
  dbt_models:
    - qualtrics_nps_scores

- name: Active Hosts
  base_path: "/handbook/product/performance-indicators/"
  definition: The count of active <a href="https://about.gitlab.com/pricing/#self-managed">Self
    Hosts</a>, Core and Paid, plus GitLab.com. Owner of this KPI is VP, Product Management.
    This is measured by counting the number of unique GitLab instances that send us
    <a href="https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html">usage
    ping</a>. We know from a <a href="https://app.periscopedata.com/app/gitlab/545874/Customers-and-Users-with-Usage-Ping-Enabled?widget=7126513&udv=937077">previous
    analysis</a> that only ~30% of licensed instances send us usage ping at least. We are planning to update the methodology to track opt-in rate more accurately.
    once a month.
  target:
  org: Product
  public: true
  is_key: false
  health:
    level: 3
    reasons:
    - See notes in <a href="https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg?usp=sharing">Key Review slides</a>
  sisense_data:
    chart: 7441303
    dashboard: 527913
    embed: v2

- name: New hire location factor
  base_path: "/handbook/product/performance-indicators/"
  parent: "/handbook/people-group/people-success-performance-indicators/"
  target: Our Target is 0.72.
  org: Product
  public: true
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in <a href="https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg?usp=sharing">Key Review slides</a>
  sisense_data:
    chart: 6838981
    dashboard: 527913
    embed: v2
  sisense_data_secondary:
    chart: 7154820
    dashboard: 551165
    embed: v2

- name: Refunds processed as % of orders
  base_path: "/handbook/product/performance-indicators/"
  definition: For a month take the number of refunds and divide that by the total
    amount of orders (including self-managed and sales initiated orders). This
    does not include Internal Refunds which are results from when a web direct
    opportunity from a closed month is not credited to the correct sales person.
    Internal Refunds are not refunds to customers, therefore not meaningful when
    analyzing refunds processed as orders from GitLab's revenue system, Zuora.
  target:
  org: Product
  public: true
  is_key: false
  health:
    level: 2
    reasons:
    - See notes in <a href="https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg?usp=sharing">Key Review slides</a>
  sisense_data:
    chart: 7720228
    dashboard: 527913
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=8131064&udv=0

- name: Acquisition impact
  base_path: "/handbook/product/performance-indicators"
  definition: Acquisition impact measures the number of <a href="https://about.gitlab.com/direction/maturity/">maturity
    levels</a> advanced, as a result of acquisitions. The target is 3 per year. (<a href="https://app.periscopedata.com/app/gitlab/734231/Category-Maturity-Achievement-and-Acquisition-Impact">Advancements by acqusition can be seen here</a>)
  target: 3 per year
  org: Product
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - TBD
  sisense_data:
    chart: 10055509
    dashboard: 734231
    embed: v2  

- name: Percentage of transactions through self-service purchasing
  base_path: "/handbook/product/performance-indicators"
  definition: Total transactions processed through the webstore, compared to transactions processed outside the webstore. This measure includes SaaS and self-managed subscriptions, additional CI minutes, storage, upgrading tiers and adding users. Driving adoption of self-service purchasing allows us to redirect manual sales efforts towards higher return activities.
  target: 85%
  org: Product
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - Dashboard has been updated to include all transactions (not just SMB) and our target of 85%
  sisense_data:
    chart: 10330843
    dashboard: 779889
    embed: v2  

- name: Stage Monthly Active Users (SMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: Stage Monthly Active Users is required for all product stages. SMAU
    is defined as the count of unique users who performed a particular
    action, or set of actions, within a stage in a 28 day rolling period
    for Self-Managed and SaaS. Numbers displayed below are for self-managed instances
    with usage ping (<a href="https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=8462059&udv=0">enabled</a>),
    including the self-managed instance hosting the SaaS product.
    All (<a href="https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=8131064&udv=0">SMAU for SaaS</a>)
    is available separately using GitLab.com Postgres Database Import.
    Owner of this KPI is the VP, Product Management.

  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 2
    reasons:
    - The [Stage and Group Performance Indicators Page](https://about.gitlab.com/handbook/product/stage-and-group-performance-indicators/)
    - The [Dev Section Performance Indicator Page](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/)
    - The [Sec Section Performance Indicator Page](https://about.gitlab.com/handbook/product/sec-section-performance-indicators/)
    - The [Ops Section Performance Indicator Page](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/)
    - See notes in <a href="https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg?usp=sharing">Key Review slides</a>
    - The SMAU candidate for each stage is selected by evaluation of Usage Ping data.
    - The SMAU for the Manage Stage (Owner - @ljlane) is the number of unique users
      viewing any [Analytics page](https://docs.gitlab.com/ee/user/analytics/).
    - The SMAU for the  Plan Stage (Owner - @gweaver) is the number of unique
      authors who create issues (Issue.distinct_count_by(author_id)).
    - The SMAU for the  Create Stage (Owner - @sarahwaldner) is the number of unique
      users who performed a repository write operation.
    - The SMAU for the  Verify Stage (Owner - @jreporter) is the number of unique
      users who trigger ci_pipelines (Ci.pipeline.distinct_count_by(user_id)).
    - The SMAU for the  Package Stage (Owner - @trizzi ) is the number of unique users who publish packages to the registry.
    - The SMAU for the  Secure Stage (Owner - @david) is the number of unique users
      who have used one or more of the Secure scanners. This is tracked with the `user_unique_users_all_secure_scanners`
      statistic in usage ping. [More details on AMAU](/handbook/product/performance-indicators/#action-monthly-active-users-amau)
    - The SMAU for the  Release Stage (Owner - @ogolowinski) is the number of unique
      users who trigger deployments (Deployment.distinct_count_by(user_id)).
    - The SMAU for the  Configure Stage (Owner - @nagyv-gitlab) will be a proxy metric
      of all the active project users for any projects with a cluster attached. [More
      info](https://gitlab.com/gitlab-org/growth/product/-/issues/1526)
    - The SMAU for the  Monitor Stage (Owner - @kbychu) will be a proxy metric of
      all the active project users for any projects with Prometheus enabled. [More
      info](https://gitlab.com/gitlab-org/growth/product/-/issues/1526)
    - The SMAU for the Protect Stage (Owner - TBD) is in the evaluation phase.
  sisense_data:
    chart: 10042348
    dashboard: 758607
    embed: v2
  data_source:
    - version_db
  dbt_models:
    - mart_monthly_product_usage
- name: Paid Stage Monthly Active Users (Paid SMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: Paid Stage Monthly Active Users is required for all product stages.
    Paid SMAU is defined as the count of SMAU (Stage Monthly Active Users) that roll
    up to paid instance for Self-Managed using Usage Ping data or a paid namespace for SaaS using GitLab.com Postgres Database Imports in a 28 day rolling period.
    The license information used to generate this metric is being validated, and the currently displayed data could
    be incorrect by up to 20%.  Numbers displayed below for self-managed are for instances
    with usage ping (<a href="https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=8462059&udv=0">enabled</a>).
    (<a href="https://app.periscopedata.com/app/gitlab/604621/GitLab.com-Postgres-SMAU?widget=9128003&udv=0">Paid SMAU for SaaS</a>)
    is available separately using GitLab.com Postgres Database Import.
    Owner of this KPI is the VP, Product Management.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - See reasons for [SMAU](/handbook/product/performance-indicators/#stage-monthly-active-users-smau)
  sisense_data:
    chart: 8887064
    dashboard: 634200
    embed: v2
  data_source:
    - version_db
    - gitlab_dotcom
  dbt_models:
    - mart_monthly_product_usage
    - gitlab_dotcom_daily_usage_data_events

- name: Stage Monthly Active Clusters (SMAC)
  base_path: "/handbook/product/performance-indicators/"
  definition: In categories where there is not a directly applicable SMAU metric as
    the category is focused on protecting our customer's customer, other metrics need
    to be reviewed and applied.  An example of a group where this applies is [Protect's
    Container Security group](/handbook/product/categories/#container-security-group)
    where their categories are designed to protect our customers and by extension
    their customers who are actively interacting with our customer's production /
    operations environment.  Metrics such as stage monthly active clusters (SMAC)
    can be used in place of SMAU until it is possible to achieve an accurate metric
    of true SMAU which may include measurements of monthly active sessions into the
    customer's clusters.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
- name: Group Monthly Active Users (GMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: GMAU is defined as the count of unique users who performed a particular
    action, or set of actions, within a group in a 28 day rolling period. Each R&D
    group is expected to have a primary performance indicator they are using to gauge
    the impact of their group on customer outcomes.  Where possible, groups should
    use Group Monthly Active Users (GMAU) or Group Monthly Active Paid Users (GMAPU)
    as their primary performance indicator.  GMAU should be used when the group is
    early in its maturity and its primary goal is new user adoption.  Paid GMAU should
    be the primary performance indicator when groups are more mature and have proven
    an ability to drive incremental IACV.  We prefer measuring monthly active usage
    over other potential measures like total actions taken (eg total dashboard views),
    as we believe that measuring active usage is a better measure of true user engagement
    with GitLab's product. Since R&D groups often contain more than one category,
    picking one category to base the action, or set of actions on, is a recommended
    simplification as we do not have the data bandwidth to support measuring every
    category's usage at this time.<br>The 36-month Predicted GMAU calculation <a href='https://about.gitlab.com/handbook/business-ops/data-team/data-catalog/xmau-analysis/predicted-xmau-algorithm.html'>is explained here.</a>
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - The GMAU candidates can be found on each [Product Section Performance Indicator
      Page](https://about.gitlab.com/handbook/product/performance-indicators/#other-pi-pages)
    - The [Stage and Group Performance Indicators Page](https://about.gitlab.com/handbook/product/stage-and-group-performance-indicators/)
    - The [Dev Section Performance Indicator Page](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/)
    - The [Sec Section Performance Indicator Page](https://about.gitlab.com/handbook/product/sec-section-performance-indicators/)
    - The [Ops Section Performance Indicator Page](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/)
    - The [Enablement Section Performance Indicator Page](https://about.gitlab.com/handbook/product/enablement-section-performance-indicators/)
  sisense_data:
    chart: 10789333
    dashboard: 798616
    embed: v2
- name: Paid Group Monthly Active Users (Paid GMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: Paid GMAU is defined as the count of GMAU (Group Monthly Active Users)
    that roll up to a paid instance for Self-Managed or a paid namespace for SaaS in a 28 day rolling period. When Paid GMAU is the
    focus area for a group, they should measure actions tied to`Up-tiered features`,
    which are features that are unavailable in the free tier, as this will ensure
    we are tracking usage that has a clear tie to incremental IACV.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - The Paid GMAU candidates can be found on each [Product Section Performance Indicator
      Page](https://about.gitlab.com/handbook/product/performance-indicators/#other-pi-pages)
    - The [Stage and Group Performance Indicators Page](https://about.gitlab.com/handbook/product/stage-and-group-performance-indicators/)
    - The [Dev Section Performance Indicator Page](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/)
    - The [Sec Section Performance Indicator Page](https://about.gitlab.com/handbook/product/sec-section-performance-indicators/)
    - The [Ops Section Performance Indicator Page](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/)
    - The [Enablement Section Performance Indicator Page](https://about.gitlab.com/handbook/product/enablement-section-performance-indicators/)

- name: Dropped Opportunity Canvases
  base_path: "/handbook/product/performance-indicators/"
  definition: An active and diligent product development is best evaluated by the number of features not being built. As a central tool in problem validation is the [use of opportunity canvas reviews](https://about.gitlab.com/handbook/product/product-processes/#opportunity-canvas), we can measure the number of product directions that we did not want to follow by looking at the number of the dropped or postponed opportunity canvases.
  target: 2 x number of PMs each quarter
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - As it is a new PI, it should be broadcasted around the product organisation. Moreover, we don't have automatic charting for it, just a Google Drive where all the canvases are collected.

- name: Unique Registered Non blocked Users on Active Instances
  base_path: "/handbook/product/performance-indicators/"
  definition: Monthly Number of Registered Non-blocked users on Active Instances (we define here active as an instance that sent us a usage ping on a specific month M). This PI is calculated directly from the Usage Ping Data thanks to the counter `active_user_count` (<a href="https://docs.gitlab.com/ee/development/usage_ping/dictionary.html#active_user_count">dictionary definition</a>)
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  sisense_data:
    chart: 12106908
    dashboard: 771580
    embed: v2

- name: Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per 28 day period
  org: Product
  public: true
  is_key: false
  health:
    level: 2
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [Sisense Dashboards](https://app.periscopedata.com/app/gitlab/891029/Error-Budgets-Overview)
    - The [Grafana Dashboards per Stage](https://dashboards.gitlab.net/dashboards/f/product/product)
    - The [Grafana Dashboards per Stage Group](https://dashboards.gitlab.net/dashboards/f/stage-groups/stage-groups)
  sisense_data:
    chart: 12164729
    dashboard: 891029
    embed: v2
