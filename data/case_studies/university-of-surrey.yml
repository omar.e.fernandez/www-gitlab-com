title: University of Surrey 
cover_image: '/images/university_of_surrey_cover_image.jpg'
cover_title: |
  The University of Surrey achieves top marks for collaboration and workflow management with GitLab
cover_description: |
  Learn how GitLab's stable code repository fosters collaboration and visibility at the University of Surrey


customer_logo: '/images/case_study_logos/logo_uni_surrey.png'
customer_logo_css_class: brand-logo-tall
customer_industry: Education
customer_location: UK
customer_solution: GitLab Ultimate
customer_employees: 4,000
customer_overview: |
  The University of Surrey is a leading research university dedicated to providing exceptional teaching and practical learning to over 17K students.
customer_challenge: |
  The University of Surrey sought a reliable, collaborative code repository. It used Subversion, but struggled with the lack of a single interface.

key_benefits:
  - |
    Increased collaboration and visibility
  - |
    Single user interface
  - |
    Successful workflow implementation
  - |
    Improved testing capabilities

customer_stats:
  - stat: 67 
    label: Runners serving jobs from 150 projects 
  - stat: 2300+
    label: GitLab projects on its platform 
  - stat: 110K  
    label: Builds created by 830 active users

customer_study_content:
  - title: the customer
    subtitle: A global community dedicated to education and research
    content:
      - |
        The University of Surrey is a leading research university in England, UK focused on practice-based education. Founded in 1966, The University specializes in engineering, science, and sociology. [The University of Surrey](https://www.surrey.ac.uk) teaches over 17,000 students from around the world and strives to empower its student body for personal and professional success. 
      
  - title: the challenge
    subtitle:  Inefficient file storage and lacking visibility
    content:
      - |
       The Information Technology department at the University of Surrey faced a myriad of challenges in supporting researchers to store code in an on-premise [version control system](/topics/version-control/). The team had to balance the needs of academics who asked for an enhanced version control tool coupled with their own need to collaborate and iterate in a more efficient, modern way. A growing team accompanied a more complex infrastructure, and the previous method of using Subversion or storing files in folders or on storage locations did not support the IT team or academics.  
      - |
        “We were at a point where we had to collaborate and start doing DevOps the modern way. We had automation tools, infrastructure as code, quick cycles, and testing, but we had a growing team and collaboration wasn’t easy to do,” explains Kosta Polyzos, Senior System Administrator. Compounding the difficulties was the lack of an interface to easily provide visibility into work. It was becoming increasingly difficult for colleagues to keep track of progress and make informed contributions at the right place, to the right effect. Committed to increasing collaboration, the team set out to find a solution that offered reliability and a seamless single interface to coordinate work.
    
  - blockquote: We like that GitLab is a platform that provides a one-point solution for many things. It’s not just a repository; it has many features, including Wiki, Pages, analytics, a web interface, merge requests, CI/CD, and issue comments. You can collaborate in issues and merge requests. It’s a nice package overall.
    attribution: Kosta Polyzos 
    attribution_title: Senior Systems Administrator

  - title: the solution
    subtitle: A stable, reliable code repository facilitates collaboration
    content:
      - |
       Determining a solution required factoring in several unique considerations. The main request from all stakeholders was to have a stable code repository that enabled rapid collaboration. Because the University of Surrey has strict regulations regarding data, the team also needed an on-premise solution to ensure full compliance. Academics may not have strong Git skills, so the solution also needed to have a low barrier to entry for people to collaborate and support quick adoption.
      - |
        Considering these factors, the team identified GitLab as the solution to increase collaboration and iteration across teams regardless of role. By starting on a free tier, the team was able to try out features, and when GitLab provided [free, unlimited licenses for academic institutions](/solutions/education/), every feature became available to them. “This was very good news for us, because we have access to every GitLab feature. It doesn’t mean that everyone will use all the extra features, but they’re available to solve specific problems if someone needs a solution. I think it’s a good thing for universities and their research communities,” Polyzos said.
      - |
        GitLab has satisfied academics as well. As Polyzos explained, “GitLab’s GUI helps people understand what is going on by visualizing all things Git. I’m not sure people were particularly familiar with Git, but learning it through GitLab made it very easy to get people on board.” As Oscar Mendez Maldonado, Research Fellow, explained, “Having a university-hosted repository is crucial to ensuring access, safety, and reliability.” With GitLab, the team increased collaboration and was able to implement a streamlined workflow, bringing together contributors and breaking down silos. 

  - blockquote: GitLab does a really good job of visualizing the branch structure of a given Git repository, allowing me to quickly refer back to a previous version of code rather than checking out individual branches/hashes at a time.
    attribution: Matthew Shere 
    attribution_title: Postgraduate research student

  - title: the results
    subtitle: A modern, flexible workflow
    content:
      - |
        The University of Surrey has over 2,300 projects and 110,000 builds. These efforts are made possible by implementing a seamless workflow in which users create personal branches from issues and test solutions before merging into the main branch and deploying to the production line. This workflow has improved the way the team delivers their service and simplified their growing infrastructure. “When it comes to operations, we wouldn’t have been able to cope. GitLab’s code repository and CI are the two main things that have enabled this for us,” Polyzos said. 
      - |
        GitLab has helped the IT team write more and better code and create a DevOps model. “This modern world of IT is not a single problem with a single solution. You need many tools in your toolchain. GitLab is a core tool with a central place in our toolchain. It has enabled us to further explore other tools and combine them,” Polyzos shared. “GitLab helped us implement the workflow architecturally, and we can iterate and go through the workflow again and again to improve. It has also helped us better integrate testing into the workflow. Protected branches and merge approvals provide control over the flow of new code before CI/CD pushes out the changes. Basic syntax and linting errors have essentially disappeared, and logical errors are caught earlier in the workflow.”  
      - |
        While the IT team has made use of GitLab’s branching, testing, and coding capabilities, academics have incorporated GitLab in the teaching environment due to its GUI, which provides a simple code visualization to identify changes and authors. In a group assignment, professors find it useful to understand which students committed specific changes. “Users appreciate the seamless interface - from setting up SSH keys to navigation - so that every little detail makes the user experience really enjoyable and effortless. Someone with no Git experience whatsoever doesn’t feel intimidated by it,” Polyzos said.
      - |
        As the IT team and academics tinkered with GitLab to discover the features that meet their needs, the usefulness of GitLab’s applications development features has slowly spread throughout the University of Surrey. "GitLab is a reliable tool and code repository first and foremost. It lubricates workflows in various ways and allows for collaboration internally as well as externally," Polyzos added.
      - | 
        Some users have built custom Docker images by uploading files and setting GitLab runners to build the images for them, while others who require a website now use GitLab Pages. As Dean Roe, System Administrator Team Leader, said “We are continually impressed at the rate at which GitLab expands its feature set in useful ways. The blog posts on gitlab.com do an excellent job of telling us and our users what fixes and new features we can expect in each new release. The process of bringing in updates is effortless too.
      - |
        By using GitLab, the University of Surrey has increased collaboration, while developing higher quality code through a robust solution to deliver better software faster. “We like GitLab, and the users like it. It’s stable and trustworthy, and we’re definitely going to continue using it.”       
   
      - |
        ## Learn more about GitLab for education
      - |
        [Dublin City empowers students for the IT industry](/customers/dublin-city-university/)
      - |
        [Increasing development speed at EAB](/customers/EAB/)
      - |
        [How the University of Washington improved project management](/customers/uw/)
