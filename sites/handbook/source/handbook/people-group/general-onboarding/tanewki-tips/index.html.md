---
layout: handbook-page-toc
title: TaNewKi Tips
description: A guide for new team members
---

## On this page

- TOC
{:toc .hidden-md .hidden-lg}


## <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> Welcome!
{: #tanuki-orange}

Hello! We could not be more excited to have you at GitLab. This page is here to help walk you through what you can expect before and during onboarding as a new team member. 


## <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> Meet the People Experience Team! 
{: #tanuki-orange}

[The People Experience Team](https://about.gitlab.com/handbook/people-group/people-experience-team/) is here to help guide you in your journey here at GitLab. A memeber of the team will be assigned to your Onboarding Issue, and will be there to provide guidance as you begin onboarding. You can learn more about us below by reading our bios on the team page. 

- [Alex Venter, People Experience Associate](https://about.gitlab.com/company/team/#alex_venter)
- [Ashley Jones, Sr. People Experience Associate](https://about.gitlab.com/company/team/#asjones)
- [Emily Mowry, Sr. People Experience Associate](https://about.gitlab.com/company/team/#Mowry)
- [Beverley Rufener, Manager, People Operations](https://about.gitlab.com/company/team/#brufener)


## <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> Timeline 
{: #tanuki-orange}
After you sign your offer and complete your background check and references, you can expect your onboarding to go like this: 

##### Before you start
{: .gitlab-orange}
1. Sign your offer, and be on the lookout for your :email: Welcome Email from the Candidate Experience Specialist team. You can read about the team on [this handbook page](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/coordinator/).
    - This email will contain all pertainent information such as how to order your office equipment and most importantly your laptop. The IT team also has a very handy, handbook page [you can review](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/#laptops).
    - Please note,when you receive your laptop, make sure to **not** sign into the laptop with your personal Apple ID. You will be asked to create an Apple ID on day one with your GitLab email address. 
1. If you are being employed by one of GitLab's US entities, either GitLab, Inc or GitLab, Fed, you will need to [complete your I9 via LawLogix](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#timing-of-i-9). You will receive an email from LawLogix. This email will walk you through the process of completing the I9. 
1. If you live in Germany, South Korea, Japan, France, or the UK you will need to be on a lookout from an email from the People Experience Team (via DocuSign or directly from the team) to complete some payroll documents. 
1. The next email you will receive, is a [TaNEWki call](https://about.gitlab.com/handbook/people-group/general-onboarding/#tanuki-orange) invite. We hope you can join us! This Zoom call addresses those first day nerves and gives you some time to meet other new team members. You will see this invite sent to you 1-2 weeks before your start date. 
1. You may receive an email from Okta (our SAAS application portal) with an invite to register. **Please ignore this email** until you receive your onboarding welcome email on your first day with us. This will ensure that no issues are experienced with your access levels on your first day. 

##### First day
{: .gitlab-orange}

1. **When should you log on for day one?** Unless you have worked out a specific time with your manager, feel free to start your day when you feel comfortable starting. We love non-linear work days but you have to [find what works for you](https://about.gitlab.com/company/culture/all-remote/non-linear-workday/#find-what-works-for-you).
1. The first thing you'll want to do is check your personal email where you will have received an onboarding email from GitLab with directions on how to access Okta, your GitLab email and all other applications. Most of GitLab applications are accessed through Okta so make sure you follow the directions in the email in order. 
1. Once you accept your GitLab access invites, you'll be able to open up and see your own personalized version of the [onboarding issue](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/master/.gitlab/issue_templates/onboarding.md). 


## <i class="fab fa-gitlab fa-fw" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i> Onboarding Expectations
{: #tanuki-purple}  

1. Take your time! There is a lot to go through, and you may feel like you have to get to everything each day. If you do great! If not, that's great too.    
 - The entire onboarding issue is self-paced however, anything marked with a big red dot - 🔴 - should be completed with importance on the assigned day it is under.
1. You are going to be overwhelmed when looking at your onboarding issue. This feeling is completely normal. But remember that your manager, onboarding buddy, assigned People Experience Associate, and the entire company is here to help you if you need it. 
    - Two of the best Slack channels to get yourself into to ask questions are `#it-help` and `#people-connect`

{::options parse_block_html="true" /}

<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>&nbsp;&nbsp;
Want to learn more about Onboarding from current team members? View their feedback [here](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-feedback/)!
&nbsp;&nbsp;<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>
{: .alert .alert-webcast}

