---
layout: handbook-page-toc
title: GitLab's Customer Assurance Package
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# GitLab's Customer Assurance Package

At GitLab, we believe that [transparency](/handbook/values/#transparency) is critical to our success- and security is no different. Our **Customer Assurance Package (CAP)** is designed to provide GitLab team members, users, customers, and other community members with the most current information about our Security and Compliance Posture.

## Self Service Resources

We encourage our Customers and Prospects to begin by reviewing our Security Assurance self-service resources below.

### GitLab's Information Security Policies

- [Business Continuity Plan](/handbook/business-ops/gitlab-business-continuity-plan/)
- [Disaster Recovery](https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/library/disaster-recovery/index.md)
- [Endpoint Management](/handbook/business-ops/team-member-enablement/onboarding-access-requests/endpoint-management/)
- [Encryption Policy](/handbook/engineering/security/vulnerability_management/encryption-policy.html)
- [Data Classification Policy](/handbook/engineering/security/data-classification-standard.html)
- [Governance](/handbook/board-meetings/bylaws.html)
- [Access Management Policy](/handbook/engineering/security/access-management-policy.html)
- [Security Incident Response Guide](/handbook/engineering/security/security-operations/sirt/sec-incident-response.html)
- [Acceptable Use Policy](/handbook/people-group/acceptable-use-policy/)
- [Audit Logging Policy](/handbook/engineering/security/audit-logging-policy.html)
- [Security Operational Risk Management](/handbook/engineering/security/security-assurance/risk-field-security/operational-risk-management-methodology.html)
- [Privacy Policy](/handbook/legal/privacy/)
- [Security Awareness Training](/handbook/security/#security-awareness-training)
- [Third Party Risk Management](/handbook/engineering/security/security-assurance/risk-field-security/third-party-risk-management.html)
- [Vulnerability Management Overview](/handbook/engineering/security/vulnerability_management/#vulnerability-management-overview)
- [Password Policy](/handbook/security/#gitlab-password-policy-guidelines)
- [Data Protection Impact Assessment Policy](/handbook/engineering/security/dpia-policy/)
- [Penetration Testing Policy](/handbook/engineering/security/penetration-testing-policy.html)

### GitLab Architecture

- [Application Architecture](https://docs.gitlab.com/ee/development/architecture.html)
- [Infrastructure Architecture](/handbook/engineering/infrastructure/environments/)
- [High-level Network Diagram](/handbook/engineering/infrastructure/production/architecture/#network-architecture)
- [Blog Post: Securing your Instance Best Practices](/blog/2020/05/20/gitlab-instance-security-best-practices/)
- [Technical Report: Securing Customer Data](/handbook/engineering/security/security-assurance/risk-field-security/technical-reports/securing-customer-data.pdf)

### GitLab's Security Compliance

- [GitLab Security Trust Center](/security/)
- [GitLab Security Control Framework](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html)
- [Security Certifications and Attestations](/handbook/engineering/security/security-assurance/security-compliance/certifications.html)

### Assurance Documentation- No NDA Required

- [GitLab's SOC 3 Report](https://gitlab.com/gitlab-com/gl-security/soc-3-project)
- [Cloud Security Alliance (CSA)
Consensus Assessments Initiative Questionnaire (CAIQ)](https://cloudsecurityalliance.org/star/registry/gitlab/) 
- [SIG Lite Questionnaire](/handbook/engineering/security/security-assurance/risk-field-security/technical-reports/GitLAb SIG Lite 2021 6.2021 - Final Draft.pdf)

### Assurance Documentation- NDA Required

Due to the nature of some of GitLab's security certifications and reports, the below resources are availble under a Non Discolsure Agreement. These resources can be requested using the [Customer Assurance Activities](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/customer-security-assessment-process.html) workflow or by emailing security@gitlab.com. 

- [GitLab's SOC2 Type 2 Report](https://www.aicpa.org/interestareas/frc/assuranceadvisoryservices/aicpasoc2report.html).
- [GitLab's Annual Penetration test](https://about.gitlab.com/handbook/engineering/security/#annual-3rd-party-security-testing). 
- [GitLab's PCI DSS SAQ-A](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/certifications.html#current)

### Third Party Security Rating Platforms

#### BitSight

BitSight utilizes public information collected across multiple domains to provide a numeric score from 250-900. For information on how GitLab maintains this score, refer to our [Third Party Security Rating Platforms](/handbook/engineering/security/security-assurance/risk-field-security/security-rating-platforms.html) handbook page. GitLab provides a quarterly company preview report that can be accessed using the links in the drop down section below. The company preview report provides a summary of GitLab's BitSight Score for the production GitLab SaaS offering. We additionally include historical reports for anyone interested in seeing changes to our score quarter over quarter. Note that the company preview report is automatically refreshed by BitSight on a quarterly basis and GitLab does not have the ability to pull a report in real time. The report is refreshed on the first day of the month following a quarter (i.e. the Q1 report for calendar year 2021 is generated on 2021-04-01).

<details markdown="1">
<summary><b>GitLab's Quarterly Comapny Preview Reports from BitSight - Download Links</b></summary>

In the table below you will find links to quarterly company preview reports generated by BitSight for GitLab's Production SaaS. The most current report available will always be at the top of the table.

|Calendar Year-Quarter|Link to GitLab Company Preview Report|
|:-----:|:----------:|
|2021-Q2|[GitLab Company Preview Report CY21Q2](/handbook/engineering/security/security-assurance/risk-field-security/technical-reports/BitSightJul2021.pdf)|
|2021-Q1|[GitLab Company Preview Report CY21Q1](/handbook/engineering/security/security-assurance/risk-field-security/technical-reports/BitSightApr2021.pdf)|

</details>

## Additional Support

If you have any further questions that aren't answered here, please follow the below steps:

**Prospective Customers**: Please [fill out a request](https://about.gitlab.com/sales/) and a representative will reach out to you.

**Current Customers**: Please contact your [Account Owner](/handbook/sales/#initial-account-owner---based-on-segment) at GitLab. If you don't know who that is, please [reach out to sales](https://about.gitlab.com/sales/) and ask to be connected to your Account Owner.

**GitLab Team Members**: Contact the Risk and Field Security team using the **Customer Assurance** workflow in the slack [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70) channel. 

---
Coming soon: Regulated Markets Customer Assurance Package
