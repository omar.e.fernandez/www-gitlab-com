---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY22

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 1 | 1080 |
| [@manojmj](https://gitlab.com/manojmj) | 2 | 960 |
| [@engwan](https://gitlab.com/engwan) | 3 | 780 |
| [@theoretick](https://gitlab.com/theoretick) | 4 | 700 |
| [@leipert](https://gitlab.com/leipert) | 5 | 580 |
| [@alexpooley](https://gitlab.com/alexpooley) | 6 | 500 |
| [@pks-t](https://gitlab.com/pks-t) | 7 | 500 |
| [@mksionek](https://gitlab.com/mksionek) | 8 | 460 |
| [@stanhu](https://gitlab.com/stanhu) | 9 | 400 |
| [@whaber](https://gitlab.com/whaber) | 10 | 400 |
| [@djadmin](https://gitlab.com/djadmin) | 11 | 400 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 12 | 400 |
| [@10io](https://gitlab.com/10io) | 13 | 380 |
| [@markrian](https://gitlab.com/markrian) | 14 | 360 |
| [@mrincon](https://gitlab.com/mrincon) | 15 | 340 |
| [@ali-gitlab](https://gitlab.com/ali-gitlab) | 16 | 340 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 17 | 320 |
| [@sabrams](https://gitlab.com/sabrams) | 18 | 300 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 19 | 300 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 20 | 300 |
| [@mikeeddington](https://gitlab.com/mikeeddington) | 21 | 300 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 22 | 250 |
| [@tkuah](https://gitlab.com/tkuah) | 23 | 240 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 24 | 230 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 25 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 26 | 200 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 27 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 28 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 29 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 30 | 200 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 31 | 200 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 32 | 140 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 33 | 140 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 34 | 140 |
| [@kerrizor](https://gitlab.com/kerrizor) | 35 | 130 |
| [@twk3](https://gitlab.com/twk3) | 36 | 130 |
| [@seanarnold](https://gitlab.com/seanarnold) | 37 | 120 |
| [@mwoolf](https://gitlab.com/mwoolf) | 38 | 110 |
| [@balasankarc](https://gitlab.com/balasankarc) | 39 | 110 |
| [@allison.browne](https://gitlab.com/allison.browne) | 40 | 100 |
| [@cablett](https://gitlab.com/cablett) | 41 | 100 |
| [@vsizov](https://gitlab.com/vsizov) | 42 | 80 |
| [@splattael](https://gitlab.com/splattael) | 43 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 44 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 45 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 46 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 47 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 48 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 49 | 80 |
| [@mkozono](https://gitlab.com/mkozono) | 50 | 80 |
| [@dblessing](https://gitlab.com/dblessing) | 51 | 80 |
| [@tancnle](https://gitlab.com/tancnle) | 52 | 80 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 53 | 80 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 54 | 70 |
| [@mbobin](https://gitlab.com/mbobin) | 55 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 56 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 57 | 60 |
| [@jannik_lehmann](https://gitlab.com/jannik_lehmann) | 58 | 60 |
| [@nfriend](https://gitlab.com/nfriend) | 59 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 60 | 50 |
| [@tomquirk](https://gitlab.com/tomquirk) | 61 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 62 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 63 | 40 |
| [@dgruzd](https://gitlab.com/dgruzd) | 64 | 40 |
| [@f_caplette](https://gitlab.com/f_caplette) | 65 | 40 |
| [@proglottis](https://gitlab.com/proglottis) | 66 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 67 | 30 |
| [@cngo](https://gitlab.com/cngo) | 68 | 30 |
| [@ekigbo](https://gitlab.com/ekigbo) | 69 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 70 | 30 |
| [@lulalala](https://gitlab.com/lulalala) | 71 | 30 |
| [@dzaporozhets](https://gitlab.com/dzaporozhets) | 72 | 30 |
| [@ifarkas](https://gitlab.com/ifarkas) | 73 | 30 |
| [@nmezzopera](https://gitlab.com/nmezzopera) | 74 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 5 | 80 |
| [@smcgivern](https://gitlab.com/smcgivern) | 6 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 7 | 40 |
| [@rspeicher](https://gitlab.com/rspeicher) | 8 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@lienvdsteen](https://gitlab.com/lienvdsteen) | 1 | 600 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@JeremyWuuuuu](https://gitlab.com/JeremyWuuuuu) | 1 | 600 |
| [@leetickett](https://gitlab.com/leetickett) | 2 | 500 |
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 3 | 300 |
| [@tnir](https://gitlab.com/tnir) | 4 | 200 |

## FY22-Q2

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@manojmj](https://gitlab.com/manojmj) | 1 | 900 |
| [@pks-t](https://gitlab.com/pks-t) | 2 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 3 | 400 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 4 | 400 |
| [@10io](https://gitlab.com/10io) | 5 | 380 |
| [@leipert](https://gitlab.com/leipert) | 6 | 380 |
| [@markrian](https://gitlab.com/markrian) | 7 | 360 |
| [@ali-gitlab](https://gitlab.com/ali-gitlab) | 8 | 340 |
| [@mksionek](https://gitlab.com/mksionek) | 9 | 320 |
| [@mrincon](https://gitlab.com/mrincon) | 10 | 300 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 11 | 300 |
| [@mikeeddington](https://gitlab.com/mikeeddington) | 12 | 300 |
| [@theoretick](https://gitlab.com/theoretick) | 13 | 300 |
| [@stanhu](https://gitlab.com/stanhu) | 14 | 300 |
| [@tkuah](https://gitlab.com/tkuah) | 15 | 240 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 16 | 200 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 17 | 120 |
| [@engwan](https://gitlab.com/engwan) | 18 | 100 |
| [@mkozono](https://gitlab.com/mkozono) | 19 | 80 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 20 | 80 |
| [@dblessing](https://gitlab.com/dblessing) | 21 | 80 |
| [@tancnle](https://gitlab.com/tancnle) | 22 | 80 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 23 | 80 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 24 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 25 | 70 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 26 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 27 | 60 |
| [@jerasmus](https://gitlab.com/jerasmus) | 28 | 60 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 29 | 60 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 30 | 60 |
| [@jannik_lehmann](https://gitlab.com/jannik_lehmann) | 31 | 60 |
| [@nfriend](https://gitlab.com/nfriend) | 32 | 60 |
| [@dgruzd](https://gitlab.com/dgruzd) | 33 | 40 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 34 | 40 |
| [@f_caplette](https://gitlab.com/f_caplette) | 35 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 36 | 40 |
| [@kerrizor](https://gitlab.com/kerrizor) | 37 | 30 |
| [@ekigbo](https://gitlab.com/ekigbo) | 38 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 39 | 30 |
| [@lulalala](https://gitlab.com/lulalala) | 40 | 30 |
| [@dzaporozhets](https://gitlab.com/dzaporozhets) | 41 | 30 |
| [@ifarkas](https://gitlab.com/ifarkas) | 42 | 30 |
| [@nmezzopera](https://gitlab.com/nmezzopera) | 43 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rspeicher](https://gitlab.com/rspeicher) | 1 | 30 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 2 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@lienvdsteen](https://gitlab.com/lienvdsteen) | 1 | 600 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@JeremyWuuuuu](https://gitlab.com/JeremyWuuuuu) | 1 | 600 |
| [@leetickett](https://gitlab.com/leetickett) | 2 | 500 |
| [@tnir](https://gitlab.com/tnir) | 3 | 200 |

## FY22-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 1 | 1000 |
| [@engwan](https://gitlab.com/engwan) | 2 | 680 |
| [@alexpooley](https://gitlab.com/alexpooley) | 3 | 500 |
| [@theoretick](https://gitlab.com/theoretick) | 4 | 400 |
| [@whaber](https://gitlab.com/whaber) | 5 | 400 |
| [@sabrams](https://gitlab.com/sabrams) | 6 | 300 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 7 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 8 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 9 | 250 |
| [@leipert](https://gitlab.com/leipert) | 10 | 200 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 11 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 12 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 13 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 14 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 15 | 140 |
| [@mksionek](https://gitlab.com/mksionek) | 16 | 140 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 17 | 140 |
| [@twk3](https://gitlab.com/twk3) | 18 | 130 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 19 | 110 |
| [@balasankarc](https://gitlab.com/balasankarc) | 20 | 110 |
| [@stanhu](https://gitlab.com/stanhu) | 21 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 22 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 23 | 100 |
| [@cablett](https://gitlab.com/cablett) | 24 | 100 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 25 | 80 |
| [@vsizov](https://gitlab.com/vsizov) | 26 | 80 |
| [@splattael](https://gitlab.com/splattael) | 27 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 28 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 29 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 30 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 31 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 32 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 33 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 34 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 35 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 36 | 60 |
| [@manojmj](https://gitlab.com/manojmj) | 37 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 38 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 39 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 40 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 41 | 50 |
| [@tomquirk](https://gitlab.com/tomquirk) | 42 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 43 | 40 |
| [@mwoolf](https://gitlab.com/mwoolf) | 44 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 45 | 40 |
| [@mrincon](https://gitlab.com/mrincon) | 46 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 47 | 30 |
| [@proglottis](https://gitlab.com/proglottis) | 48 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 49 | 30 |
| [@cngo](https://gitlab.com/cngo) | 50 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 5 | 50 |
| [@smcgivern](https://gitlab.com/smcgivern) | 6 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 7 | 40 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |


