---
layout: markdown_page
title: "Category Direction - Product Analytics"
description: "Product analytics tools tell you how, what, when, where, and the who."
canonical_path: "/direction/monitor/product_analytics/"
---

- TOC
{:toc}

## Introduction and how you can help
Thanks for visiting this category page on Product Analytics in GitLab. This page belongs to the Product Intelligence Group, and is maintained by [Keanon O'Keefe](https://gitlab.com/kokeefe). This direction is a work in progress and everyone can contribute. Sharing your feedback directly on issues and epics at GitLab.com is the best way to contribute. If you’re a GitLab user and have direct knowledge of your need for product analytics, we’d especially love to hear from you.

## Overview
Understanding how your users use your product, what they do with your product, when they use your product, where they are interacting with your product, who they are, and what problems they run into, are important feedback that informs future improvements.

## Mission
To improve the user experience of software.

<!-->
## Strategy

### Challenges

### Opportunities

## Target Audience and Experience

## What is Next & Why?

## Dogfooding Plan

## Pricing

## Competitive Landscape

## Analyst Landscape
-->
