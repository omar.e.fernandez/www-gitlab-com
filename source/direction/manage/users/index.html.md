---
layout: markdown_page
title: "Category Direction - Users"
description: "The concept of a user isn't a new one, but it touches a number of areas in GitLab that are critical to the success of our users. Learn more!"
canonical_path: "/direction/manage/users/"
---

- TOC
{:toc}

| **Stage** | **Maturity** | **Content Last Reviewed** |
| --- | --- | --- |
| [Manage](/direction/manage/) | [Not Applicable](/direction/maturity/) | `2021-07-22` |

## Introduction and how you can help

Thanks for visiting this category page on Users in GitLab. The Users category is part of the [Manage](https://about.gitlab.com/direction/manage/) stage. This page is being actively maintained by [Orit Golowinski](https://about.gitlab.com/company/team/#ogolowinski). This vision and direction is a work in progress and sharing your feedback directly on issues and epics on GitLab.com is the best way to contribute. 

## Overview

The concept of a user isn't a new one, but it touches a number of areas in GitLab that are critical to the success of our users. Namely:

* User management: Creating and modifying users at scale.
* User profile: User settings and how a user's profile is shown to the instance/world.

## Target audience and experience

Any user of GitLab could be considered a relevant audience, but improvements in this area likely think about two specific instances:

1. Large EE instances: These are customers with large seat counts, who need the ability to understand who is using GitLab and who is not - and have the tools needed to manage these users at scale. 
1. GitLab.com: For individual contributors, the user profile becomes your identity as a developer. While concepts like the contribution graph or project list may not be of paramount importance to a self-managed enterprise user, individual developers on GitLab.com want their profile and presence on GitLab.com to represent them.

## Maturity

As much about users in GitLab are application-specific, users are considered a non-marketing category without a [maturity level](/direction/maturity/) that can be compared to other competing solutions.

## What's next & why

**Next:** Due to other priorities for the [Manage](https://about.gitlab.com/direction/manage/) stage, we are not planning to make significant investments to the Users category in the 3-4 next milestones. 

We have been making improvements to user profiles to make them more personalized. We recently added pronouns via [gitlab#333042](https://gitlab.com/gitlab-org/gitlab/-/issues/333042) and are now making user's timezone visible via [gitlab#335459](https://gitlab.com/gitlab-org/gitlab/-/issues/335459).
 
**After:** An area of focus in the future will be to improve the experience with [Project Access Tokens](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#project-access-tokens) by pivoting to an approach centered around [Service Accounts](https://gitlab.com/gitlab-org/gitlab/-/issues/284393).
