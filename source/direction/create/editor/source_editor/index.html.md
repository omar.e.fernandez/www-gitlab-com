---
layout: markdown_page
title: "Source Editor Direction"
description: "The Source Editor is the underlying component handling code editing across GitLab"
canonical_path: "/direction/create/editor/source_editor/"
---

- TOC
{:toc}

## Source Editor

Thanks for visiting the direction page for the Source Editor, a code editor maintained by the [Editor](/handbook/product/categories/#editor-group) group. While not a singular feature or official category in itself, the Source Editor is a critical component and cornerstone of the Editor group's strategy. More information about the Editor group's priorities and direction can be found on the [Editor group direction page](/direction/create/editor/) and additional questions can be directed to Eric Schurter ([E-Mail](mailto:eschurter@gitlab.com)).

### What is the Source Editor?

The Source Editor is a thin wrapper around the open source code editor, [Monaco](https://microsoft.github.io/monaco-editor/). In early development, you may have seen it referred to as "Editor Lite." 

### Who is it for?

Anyone writing code within the GitLab UI.

### Where can I use it?

The Source Editor is currently being used as the editing component that powers Snippets, Web IDE, the Single File Editor, and the Pipeline Editor. Soon, we will be rendering the read-only view of a file in the repository using Source Editor and exploring other uses for it around GitLab.